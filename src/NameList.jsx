import React,{useState} from 'react';

const NameList = (props) =>{
    const {items}=props;
    const [name, setName] = useState("");

    if(items.length===0){
        return <p>Lista Üres</p>
    }

    const handleChange = (event)=>{
        setName(event.target.value);
    }
    
    const filtered = () =>{
        if(name === "") return items;
        return items.filter(item => item.startsWith(name));
    }

    return (
    <div>
        <input type="text" value={name} onChange={handleChange} /> 
        <ul>
            {filtered().map(name=>
                <li>{name}</li>
            )}
        </ul>
        <p>Elemek száma: {items.length}</p>
    </div>)
}

export default NameList;