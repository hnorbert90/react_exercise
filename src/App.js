import logo from './logo.svg';
import './App.css';
import NameManager from './NameManager';

function App() {
  return (
    <div className="App">
      <NameManager/>
    </div>
  );
}

export default App;
