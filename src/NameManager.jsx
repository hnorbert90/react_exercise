import React,{useState} from 'react';
import AddName from './AddName';
import NameList from './NameList';

const NameManager = (props) =>{
    const [names, setNames]=useState([]);

    const addName=(name)=>{
        setNames((actualNames) => [...actualNames, name]);
    }

    return(
    <div>
        <NameList items={names}/>
        <AddName onSubmit={addName}/>
    </div>
    )
}

export default NameManager;