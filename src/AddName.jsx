import React,{useState} from 'react';


const AddName = (props) =>{
    const {onSubmit}=props;
    const [name, setName] = useState("");

    const handleChange=(event)=>{
        setName(event.target.value);
    }

    const add=()=>{
        onSubmit(name);
        setName("");
    }

    return (
    <div>
        <input type="text" placeholder="name" value={name} onChange={handleChange}/>
        <button onClick={add} disabled={name===""}>Hozzáadás</button>
    </div>
        )
}

export default AddName;